package com.manong.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.manong.entity.User;
import com.baomidou.mybatisplus.extension.service.IService;
import com.manong.vo.query.UserQueryVo;

import java.util.List;

public interface UserService extends IService<User> {
    /**
     * 根据用户名查询用户信息
     */
    User findUserByUserName(String username);
    /**
     * 分页查询用户信息
     */
    IPage<User> findUserListByPage(IPage<User> page, UserQueryVo userQueryVo);
    /**
     * 删除用户信息
     */
    boolean deleteById(Long id);
    /**
     * 分配角色
     */
    boolean saveUserRole(Long userId, List<Long> roleIds);
}
