package com.manong.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.manong.entity.DictType;
import com.baomidou.mybatisplus.extension.service.IService;
import com.manong.vo.query.DictTypeQueryVo;

public interface DictTypeService extends IService<DictType> {

    IPage<DictType> findDictTypeListByPage(IPage<DictType> page, DictTypeQueryVo dictTypeQueryVo);

    boolean checkDictType(String dictType);
}
