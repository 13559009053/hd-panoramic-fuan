package com.manong.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.manong.entity.Role;
import com.baomidou.mybatisplus.extension.service.IService;
import com.manong.vo.query.RoleQueryVo;

import java.util.List;

public interface RoleService extends IService<Role> {
    /**
     * 根据用户查询角色列表
     * @param page    
     * @param roleQueryVo    
     * @return
     */
    IPage<Role> findRoleListByUserId(IPage<Role> page, RoleQueryVo roleQueryVo);
    /**
     * 保存角色权限关系
     */
    boolean saveRolePermission(Long roleId, List<Long> permissionIds);
    /**
     * 根据用户ID查询该用户拥有的角色ID
     */
    List<Long> findRoleIdByUserId(Long userId);

    /**
     * 检查角色是否被分配给用户
     * @param id
     * @return
     */
    boolean checkRole(Long id);

    /**
     * 删除角色
     * @param id
     * @return
     */
    boolean deleteById(Long id);
}
