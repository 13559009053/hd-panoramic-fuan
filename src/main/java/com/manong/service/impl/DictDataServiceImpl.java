package com.manong.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.manong.entity.DictData;
import com.manong.dao.DictDataMapper;
import com.manong.entity.User;
import com.manong.service.DictDataService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.manong.vo.query.DictDataQueryVo;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.util.List;

@Service
public class DictDataServiceImpl extends ServiceImpl<DictDataMapper, DictData> implements DictDataService {

    @Override
    public IPage<DictData> findDictDataListByPage(IPage<DictData> page, DictDataQueryVo dictDataQueryVo) {
        //创建条件构造器对象
        QueryWrapper<DictData> queryWrapper = new QueryWrapper<DictData>();
        //字典类型
        queryWrapper.eq(!ObjectUtils.isEmpty(dictDataQueryVo.getDictType()),"dict_type",dictDataQueryVo.getDictType());
        //字典标签
        queryWrapper.like(!ObjectUtils.isEmpty(dictDataQueryVo.getDictLabel()),"dict_label",dictDataQueryVo.getDictLabel());
        //字典状态
        queryWrapper.eq(!ObjectUtils.isEmpty(dictDataQueryVo.getStatus()),"status",dictDataQueryVo.getStatus());
        //查询并返回数
        return baseMapper.selectPage(page,queryWrapper);
    }

    @Override
    public List<DictData> findDictDataByDictType(String dictType) {
        //创建条件构造器对象
        QueryWrapper<DictData> queryWrapper = new QueryWrapper<DictData>();
        //字典类型
        queryWrapper.eq(!ObjectUtils.isEmpty(dictType),"dict_type",dictType);
        return baseMapper.selectList(queryWrapper);
    }

}
