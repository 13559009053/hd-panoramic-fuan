package com.manong.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.manong.entity.DictType;
import com.manong.dao.DictTypeMapper;
import com.manong.service.DictTypeService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.manong.vo.query.DictTypeQueryVo;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

@Service
public class DictTypeServiceImpl extends ServiceImpl<DictTypeMapper, DictType> implements DictTypeService {

    @Override
    public IPage<DictType> findDictTypeListByPage(IPage<DictType> page, DictTypeQueryVo dictTypeQueryVo) {
        //创建条件构造器对象
        QueryWrapper<DictType> queryWrapper = new QueryWrapper<DictType>();
        //字典名称
        queryWrapper.like(!ObjectUtils.isEmpty(dictTypeQueryVo.getDictName()),"dict_name",dictTypeQueryVo.getDictName());
        //字典类型
        queryWrapper.like(!ObjectUtils.isEmpty(dictTypeQueryVo.getDictType()),"dict_type",dictTypeQueryVo.getDictType());
        //字典状态
        queryWrapper.eq(!ObjectUtils.isEmpty(dictTypeQueryVo.getStatus()),"status",dictTypeQueryVo.getStatus());
        //大于时间
        queryWrapper.ge(!ObjectUtils.isEmpty(dictTypeQueryVo.getBeginTime()),"create_time",dictTypeQueryVo.getBeginTime());
        //小于时间
        queryWrapper.le(!ObjectUtils.isEmpty(dictTypeQueryVo.getEndTime()),"create_time",dictTypeQueryVo.getEndTime());
        //查询并返回数
        return baseMapper.selectPage(page,queryWrapper);
    }

    @Override
    public boolean checkDictType(String dictType) {
        if (baseMapper.checkDictType(dictType)>0){
            return true;
        }
        return false;
    }
}
