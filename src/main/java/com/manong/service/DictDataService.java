package com.manong.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.manong.entity.DictData;
import com.baomidou.mybatisplus.extension.service.IService;
import com.manong.entity.User;
import com.manong.vo.query.DictDataQueryVo;

import java.util.List;

public interface DictDataService extends IService<DictData> {

    IPage<DictData> findDictDataListByPage(IPage<DictData> page, DictDataQueryVo dictDataQueryVo);

    List<DictData> findDictDataByDictType(String dictType);
}
