package com.manong.utils;

public class UserThreadLocal {
    private UserThreadLocal() {
    }

    private static ThreadLocal<String> THREADLOCAL = new ThreadLocal<>();

    /**
     * 设置值
     *
     */
    public static void setCurrentUserName(String username) {
        THREADLOCAL.set(username);
    }

    /**
     * 获取值
     *
     * @return
     */
    public static String getCurrentUserName() {
        return THREADLOCAL.get();
    }

    public static void remove() {
        THREADLOCAL.remove();
    }
}
