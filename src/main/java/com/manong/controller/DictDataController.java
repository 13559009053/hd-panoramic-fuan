package com.manong.controller;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.manong.entity.DictData;
import com.manong.service.DictDataService;
import com.manong.utils.Result;
import com.manong.vo.query.DictDataQueryVo;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping("/api/dictData")
public class DictDataController {
    @Resource
    private DictDataService dictDataService;
    /**
     * 查询所有字典数据列表
     */
    @GetMapping("/listAll")
    public Result listAll() {
        return Result.ok(dictDataService.list());
    }
    /**
     * 分页查询字典数据列表
     */
    @GetMapping("/list")
    @PreAuthorize("hasAuthority('sys:dictData:select')")
    public Result list(DictDataQueryVo dictDataQueryVo) {
        //创建分页信息
        IPage<DictData> page = new Page<DictData>(dictDataQueryVo.getPageNo(), dictDataQueryVo.getPageSize());
        //调用分页查询方法
        dictDataService.findDictDataListByPage(page, dictDataQueryVo);
        //返回数据
        return Result.ok(page);
    }
    /**
     * 根据字典类型查询字典数据信息
     */
    @GetMapping("/find/{dictType}")
    public Result findDictDataByDictType(@PathVariable String dictType) {
        //调用查询方法
        List<DictData> result = dictDataService.findDictDataByDictType(dictType);
        return Result.ok(result);
    }
    /**
     * 添加字典数据
     */
    @PostMapping("/add")
    @PreAuthorize("hasAuthority('sys:dictData:add')")
    public Result add(@RequestBody DictData dictData){
        //调用保存字典数据的方法
        if(dictDataService.save(dictData)){
            return Result.ok().message("字典数据添加成功");
        }
        return Result.error().message("字典数据添加失败");
    }
    /**
     * 修改字典数据
     */
    @PutMapping("/update")
    @PreAuthorize("hasAuthority('sys:dictData:edit')")
    public Result update(@RequestBody DictData dictData){
        if(dictDataService.updateById(dictData)){
            return Result.ok().message("字典数据修改成功");
        }
        return Result.error().message("字典数据修改失败");
    }
    /**
     * 删除字典数据
     */
    @DeleteMapping("/delete/{id}")
    @PreAuthorize("hasAuthority('sys:dictData:delete')")
    public Result delete(@PathVariable Long id) {
        //调用删除字典数据的方法
        if(dictDataService.removeById(id)){
            return Result.ok().message("字典数据删除成功");
        }
        return Result.error().message("字典数据删除失败");
    }
}

