package com.manong.dao;

import com.manong.entity.DictType;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Select;

public interface DictTypeMapper extends BaseMapper<DictType> {
    /**
     * 根据用户dict_type查询该角色是否被分配给用户
     */
    @Select("select count(*) from `sys_dict_data` where dict_type = #{dictType}")
    Long checkDictType(String dictType);
}
