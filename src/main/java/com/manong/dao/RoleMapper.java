package com.manong.dao;

import com.manong.entity.Role;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface RoleMapper extends BaseMapper<Role> {
    /**
     * 删除角色权限关系
     */
    @Delete("delete from sys_role_permission where role_id = #{roleId}")
    void deleteRolePermission(Long roleId);
    /**
     * 保存角色权限关系
     */
    int saveRolePermission(Long roleId, List<Long> permissionIds);
    /**
     * 根据用户ID查询该用户拥有的角色ID
     */
    @Select("select role_id from `sys_user_role` where user_id = #{userId}")
    List<Long> findRoleIdByUserId(Long userId);
    /**
     * 根据用户role_id查询该角色是否被分配给用户
     */
    @Select("select count(*) from `sys_user_role` where role_id = #{id}")
    Long checkRole(Long id);
}
