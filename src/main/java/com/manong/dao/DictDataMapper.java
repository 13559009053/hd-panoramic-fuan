package com.manong.dao;

import com.manong.entity.DictData;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

public interface DictDataMapper extends BaseMapper<DictData> {

}
