package com.manong.vo.query;

import com.baomidou.mybatisplus.annotation.TableField;
import com.manong.entity.DictType;
import lombok.Data;

import java.util.Date;


@Data
public class DictTypeQueryVo extends DictType {
    private Long pageNo = 1L;//当前页码
    private Long pageSize = 10L;//每页显示数量
    private Date beginTime;
    private Date endTime;
}
